#include "Simulator.h"
#include "MassSpringSystem.h"
#include "FileManager.h"

FileManager cap;

Simulator::Simulator()
{
	working = false;

	wind_start = 0;
	wind_end = 0;


	mX = mY = 0;
	tX = tY = 0;
	zZ = 0;
	mEventLeft = 0;
	mEventMiddle = 0;
	mEventRight = 0;
}


Simulator::~Simulator()
{
}


void Simulator::DrawAxis()
{
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 1.0f);

	glEnd();
}

void Simulator::DrawPoints()
{

	glPointSize(3.0f);
	glBegin(GL_POINTS);
	for (int i = 0; i < MSS.point.size(); i++)
	{
		if (MSS.point[i].fixed == true) 
			glColor3f(1.0f, 0.0f, 0.0f);
		else
			glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(MSS.point[i].position.x, MSS.point[i].position.y, MSS.point[i].position.z);
	}
	glEnd();
}

void Simulator::DrawSprings()
{
	glColor3f(1.0f, 1.0f, 1.0f);

	glBegin(GL_LINES);
	for (int i = 0; i < MSS.spring.size(); i++)
	{
		if (MSS.spring[i].springType == MSS.FLEXION) continue;


		glVertex3f(MSS.spring[i].points[0]->position.x, MSS.spring[i].points[0]->position.y, MSS.spring[i].points[0]->position.z);
		glVertex3f(MSS.spring[i].points[1]->position.x, MSS.spring[i].points[1]->position.y, MSS.spring[i].points[1]->position.z);
	}
	glEnd();
}


void Simulator::Initialize()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


	MSS.Init();

	printf("Point size	: %d\nSpring size	: %d\n", MSS.point.size(), MSS.spring.size());
	printf("Pulling force	: %f N\nGravity	: %f\n\n", MSS.PullForceMax, MSS.gravity);
}

void Simulator::Update()
{
	MSS.iter++;

	MSS.WorkingWind(wind_start,wind_end);

	MSS.SpringLength();

	MSS.TotalForce();
	MSS.SpringConstraint();
	MSS.Acceleration();
	MSS.Velocity();
	MSS.Position();

	MSS.CloseBottom(MSS.floor);

}

void Simulator::Render()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	gluLookAt(MSS.eyes.x, MSS.eyes.y, MSS.eyes.z, MSS.view.x, MSS.view.y, MSS.view.z, 0.0f, 1.0f, 0.0f);

	DrawAxis();

	//glPointSize(10.0f);
	//glBegin(GL_POINTS);
	//glColor3f(1.0f, 0.0f, 0.0f);
	//glVertex3f(MSS.view.x, MSS.view.y, MSS.view.z);
	//glEnd();

	glTranslatef(MSS.view.x - zZ, MSS.view.y - zZ, MSS.view.z - zZ);
	//glTranslatef(0.0, 0.0, MSS.view.z - zZ);
	glTranslatef(tX, -tY, 0.0f);
	glRotatef(rX, 1.0f, 0.0f, 0.0f);
	glRotatef(rY, 0.0f, 1.0f, 0.0f);
	//glTranslatef(-(MSS.x_end - MSS.x_start) / 2, -(MSS.y_end - MSS.y_start) / 2, -(MSS.z_end - MSS.z_start) / 2);
	
	//glRotatef(rY, 0.0f, 1.0f, 0.0f);
	//MSS.eyes.y += 0.5f*tY;
	//tX = tY = 0.0f;



	//DrawPoints();
	DrawSprings();



	//cap.ScreenCapture();

	glutSwapBuffers();
	glutPostRedisplay();
}

void Simulator::Keyborad(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'q':
	case 'Q':
	case 27:
		exit(0);
		break;
	case 'r':
		Initialize();
		break;
	case 32:
		working = !working;
		break;
	case 'a':
		for (int i = 0; i < MSS.point.size(); i++)
		{
			if (MSS.point[i].fixed == true)
				MSS.point[i].position.x -= 0.1f;
		}
		break;
	case 's':
		for (int i = 0; i < MSS.point.size(); i++)
		{
			if (MSS.point[i].fixed == true)
				MSS.point[i].position.x += 0.1f;
		}
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Simulator::Mouse(int mouse_event, int state, int x, int y)
{
	mX = x;
	mY = y;

	switch (mouse_event)
	{
	case GLUT_LEFT_BUTTON:
		mEventLeft = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_MIDDLE_BUTTON:
		mEventMiddle = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_RIGHT_BUTTON:
		mEventRight = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Simulator::Motion(int x, int y)
{
	int diffX = x - mX;
	int diffY = y - mY;

	mX = x;
	mY = y;

	if (mEventLeft)
	{
		rX += (float) 0.1f*diffY;
		rY += (float) 0.1f*diffX;
	}
	else if (mEventRight)
	{
		tX += (float) 0.05f*diffX;
		tY += (float) 0.05f*diffY;
	}
	else if (mEventMiddle)
	{
		zZ += (float) 0.1f*diffY;
	}
}