#pragma once
#include "MassSpringSystem.h"
#include <gl/GL.H>
#include <gl/GLU.H>
#include <gl/glut.h>
#include <gl/GLAUX.H>

class Simulator
{
public:
	Simulator();
	~Simulator();

	MassSpringSystem MSS;

public:

	bool	working;
	int		wind_start;		// wind starting frame
	int		wind_end;		// wind ending frame


public:

	void Initialize();
	void Update();
	void Render();
	void Keyborad(unsigned char key, int x, int y);
	void Mouse(int mouse_event, int state, int x, int y);
	void Motion(int x, int y);

	void DrawPoints();
	void DrawSprings();
	void DrawAxis();


public:

	// mouse data (m)
	int mX, mY;
	unsigned char mEventLeft, mEventMiddle, mEventRight;

	// rotation(r) & translation(t) & zooming(z) data
	float rX, rY;
	float tX, tY;
	float zZ;
};

