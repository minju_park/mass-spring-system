#pragma once
#include "Point.h"
#include "Spring.h"
#include <vector>

using namespace std;

class MassSpringSystem
{
public:
	MassSpringSystem();
	~MassSpringSystem();

	std::vector<Point>	point;
	std::vector<Spring>	spring;

	string	version; // simulation version.
	string	HANG = "HANG",
			DROP = "DROP",
			COLLISION = "COLLISION",
			DRAW = "DRAW";
	string	STRUCTURAL = "STRUCTURAL",
			SHEAR = "SHEAR",
			FLEXION = "FLEXION";
	int		iter;
	float	toque;	// critical deformation rate
	float	x_start, x_end;
	float	y_start, y_end;
	float	z_start, z_end;
	float	pi = 3.14f;
	float	gravity;
	float	dt;
	float	floor;
	Vector	FixedPointForce;
	float	PullForceMax;


	Vector	wind_velo;		// fluid (like wind) velocity
	Vector	wind;
		bool	windy;
	Vector	eyes;			// my eyes position
	Vector	view;			// my view point

	void Init();

	Vector Gravity(Point P);
	Vector Dissipation(Point P);
	Vector Viscosity(Point P);
	void InternalForce();
	void ExternalForce();
		void PullUp();
	void TotalForce();
	void Acceleration();
	void Velocity();
	void Position();
	void SpringConstraint();

	void WorkingWind(int iter_strat, int iter_end);

	void FindLikedSpring();
	void SpringLength();

	void CloseBottom(float bottom);





	int c;
};
