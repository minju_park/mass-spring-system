#pragma once

#include <iostream>
#include <stdlib.h>
#include <Windows.h>
#include <direct.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glaux.h>
#include <gl/glut.h>

using namespace std;

class FileManager
{
public:
	int						m_bitmapIdx;
	int						m_width;
	int						m_height;

public:
	FileManager(void);
	~FileManager(void);

public:
	void					ScreenCapture();
};

