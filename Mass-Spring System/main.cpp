#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.H>
#include <gl/GLU.H>
#include <gl/glut.h>
#include <gl/GLAUX.H>
#include "Simulator.h"

Simulator MSS_simul;

void Init()
{
	MSS_simul.Initialize();
}

void Render()
{
	MSS_simul.Render();
}

void Idle()
{
	if (MSS_simul.working == true)
		MSS_simul.Update();
}

void Reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, w / h, 1.0, 300.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Keyboard(unsigned char key, int x, int y)
{
	MSS_simul.Keyborad(key, x, y);
}

void Mouse(int mouse_event, int state, int x, int y)
{
	MSS_simul.Mouse(mouse_event, state, x, y);
}

void Motion(int x, int y)
{
	MSS_simul.Motion(x, y);
}

int main(int argc, char* argv[])
{
	Init();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowPosition(500, 100);
	glutInitWindowSize(800, 800);
	glutCreateWindow("Mass Spring System by MJ");
	glutDisplayFunc(Render);
	glutReshapeFunc(Reshape);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutKeyboardFunc(Keyboard);
	glutIdleFunc(Idle);


	glutMainLoop();

	return 0;
}