#pragma once
#include <string>

using namespace std;

class Point;
class Spring
{
public:
	Spring();
	~Spring();

	Point*	points[2];
	float	initial_length;
	float	length;
	float	stiffness;

	string	springType;

};

