#pragma once
#include "Vector.h"
#include "Spring.h"
#include <vector>

using namespace std;

class Point
{
public:
	Point();
	~Point();

	std::vector<Spring*> linked_spring;

	int		point_num;
	float	mass=1.0f;
	float	damping=0.99f;
	bool	fixed=false;
	Vector	acceleration;
	Vector	velocity;
	Vector	position;
	Vector	force;
	Vector	previous;
};

