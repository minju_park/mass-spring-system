#include "MassSpringSystem.h"
#include "Vector.h"
#include <stdlib.h>
#include <string>

#define index(i,j) (i+n*(j-1)-1)

MassSpringSystem::MassSpringSystem()
{
	version = DRAW;

	x_start = 0.0f;	x_end = 20.0f;
	y_start = 0.0f;	y_end = 20.0f;
	z_start = 0.0f;	z_end = 20.0f;

	iter = 0;
	toque = 0.1f;
	dt = 0.025f;
	gravity = 9.8f;
	floor = 0.0f;
	FixedPointForce = { 0.0f, 0.0f, 0.0f };
	PullForceMax = 1000.0f;

	wind_velo = { 0.0f, 0.0f, 0.0f };
	wind = { 5.0f, 5.0f, 0.0f };

	windy = false;
}

MassSpringSystem::~MassSpringSystem()
{
}

void MassSpringSystem::Init()
{
	int	m = 0, n = 0, num = 0;	 //point matrix size [ m x n ]

	FixedPointForce.equal(0.0f);

	if (version == HANG)
	{
		eyes = { (x_end - x_start) / 2, (y_end - y_start) / 2, (x_end - x_start) + (y_end - y_start) };
		view = { (x_end - x_start) / 2, (y_end - y_start) / 2, 0.0f };
		z_end = 0.0f;
	}
	else if ((version == DROP) || (version == COLLISION) || (version == DRAW))
	{
		eyes = { (x_end - x_start)*0.5f, floor + 12.0f, (z_end - z_start)*1.25f };
		view = { (x_end - x_start)*0.5f, floor, (z_end - z_start)*0.5f };
	}
	
	point.clear();
	spring.clear();

	/* Initialze point */
	if (version == HANG)
	{
		for (float i = x_start; i <= x_end; i++)
		{
			m++;
			for (float j = y_end; j >= y_start; j--)
			{
				if (m == 1) n++;

				num++;

				Point p;

				p.point_num = num;
				p.force = { 0.0f, 0.0f, 0.0f };
				p.velocity = { 0.0f, 0.0f, 0.0f };
				p.acceleration = { 0.0f, 0.0f, 0.0f };
				p.position = { i, j, z_start };
				p.previous = { i, j, z_start };

				if (p.point_num == 1 || p.point_num == (int)((x_end - x_start + 1)*(y_end - y_start) + 1))

				point.push_back(p);
			}
		}
	}
	else if (version == DROP)
	{
		for (float i = x_start; i <= x_end; i++)
		{
			m++;
			for (float j = z_start; j <= z_end; j++)
			{
				if (m == 1) n++;

				num++;

				Point p;

				p.point_num = num;
				p.force = { 0.0f, 0.0f, 0.0f };
				p.velocity = { 0.0f, 0.0f, 0.0f };
				p.acceleration = { 0.0f, 0.0f, 0.0f };
				p.position = { i, 1.0, j };
				p.previous = { i, 1.0, j };


				if (i == x_start && j == (int)(y_end - y_start) / 2)
					p.fixed = true;

				point.push_back(p);
			}
		}
	}
	else if (version == COLLISION)
	{
		for (float i = x_start; i <= x_end; i++)
		{
			m++;
			for (float j = y_end; j >= y_start; j--)
			{
				if (m == 1) n++;

				num++;

				Point p;

				p.point_num = num;
				p.force = { 0.0f, 0.0f, 0.0f };
				p.velocity = { 0.0f, 0.0f, 0.0f };
				p.acceleration = { 0.0f, 0.0f, 0.0f };
				p.position = { i, j, z_start };
				p.previous = { i, j, z_start };

				if (p.point_num == 1 || p.point_num == (int)((x_end - x_start + 1)*(y_end - y_start) + 1))
					p.fixed = true;
				if (p.point_num == 1+(y_end-y_start+1) || p.point_num == (int)((x_end - x_start + 1)*(y_end - y_start) + 1-(y_end-y_start+1)))
					p.fixed = true;

				point.push_back(p);
			}
		}
	}
	else if (version == DRAW)
	{
		for (float i = x_start; i <= x_end; i++)
		{
			m++;
			for (float j = z_start; j <= z_end; j++)
			{
				if (m == 1) n++;

				num++;

				Point p;

				p.point_num = num;
				p.force = { 0.0f, 0.0f, 0.0f };
				p.velocity = { 0.0f, 0.0f, 0.0f };
				p.acceleration = { 0.0f, 0.0f, 0.0f };
				p.position = { i, floor, j };
				p.previous = { i, floor, j };

				c = index((x_end - x_start) / 2 + 2, (y_end - y_start) / 2 + 1);

				point.push_back(p);
			}
		}
	}

	/* Initialize spring */
	for (int i = 1; i <= m; i++)
		for (int j = 1; j <= n; j++)
		{
			Spring s;

			s.points[0] = &point[index(i, j)];

			// structral spring
			s.points[1] = &point[index(i + 1, j)];
			s.springType = STRUCTURAL;
			if (i != m) spring.push_back(s);  // ��
			s.points[1] = &point[index(i, j + 1)];
			s.springType = STRUCTURAL;
			if (j != n) spring.push_back(s); // |

			// flexion spring
			s.points[1] = &point[index(i, j + 2)];
			s.springType = FLEXION;
			if (j < n - 1) spring.push_back(s); // �Ѥ�
			s.points[1] = &point[index(i + 2, j)];
			s.springType = FLEXION;
			if (i < m - 1) spring.push_back(s); // :

			// shear spring
			s.points[1] = &point[index(i+1,j+1)];
			s.springType = SHEAR;
			if (i != m && j != n) spring.push_back(s); // \ cross
			s.points[0] = &point[index(i + 1, j)];
			s.points[1] = &point[index(i, j + 1)];
			s.springType = SHEAR;
			if (i != m && j != n) spring.push_back(s); // / cross
		}

	/* initialize initial_length & length & stiffness of spring */
	for (int i = 0; i < spring.size(); i++)
	{
		Vector gap; gap.equal(0.0f);

		gap = spring[i].points[0]->position - spring[i].points[1]->position;

		spring[i].initial_length = gap.norm();
		spring[i].length = gap.norm();
		spring[i].stiffness = 200.0f;
	}

	FindLikedSpring();
}

Vector MassSpringSystem::Gravity(Point P)
{
	Vector f = { 0.0f, 0.0f, 0.0f };

	f.y = -1.0f*P.mass*gravity;

	return f;
}

Vector MassSpringSystem::Dissipation(Point P)
{
	Vector f = { 0.0f, 0.0f, 0.0f };

	f = -P.damping*P.velocity;

	return f;
}

Vector MassSpringSystem::Viscosity(Point P)
{
	Vector f = { 0.0f, 0.0f, 0.0f };

	float viscosity_coeff = 0.5f;

	f = viscosity_coeff*(wind_velo - P.velocity);

	return{ 0.0f, 0.0f, 0.0f };
}

void MassSpringSystem::ExternalForce()
{
	for (int i = 0; i < point.size(); i++)
	{
		if (point[i].fixed == true)
		{
			point[i].force = FixedPointForce;
		}
		else 
			point[i].force = Gravity(point[i]) + Dissipation(point[i]) + Viscosity(point[i]);
	}

	if (version == DRAW) PullUp();
}

void MassSpringSystem::PullUp()
{
	point[c].force.y += 100.0f;
	//printf("%f %f %f\n", point[0].force.x, point[0].force.y, point[0].force.z);
}

void MassSpringSystem::InternalForce()
{
	/* edited ver. */
	for (int j = 0; j < spring.size(); j++)
	{
		Vector l = spring[j].points[1]->position - spring[j].points[0]->position;
		float l0 = spring[j].initial_length;
		Vector dl = l - l0*(l / l.norm());

		float stiff = spring[j].stiffness;

		spring[j].points[0]->force += stiff*dl;
		spring[j].points[1]->force -= stiff*dl;

		if (spring[j].points[0]->fixed == true)
			spring[j].points[1]->force -= 5.0f*stiff*dl;
		if (spring[j].points[1]->fixed == true)
			spring[j].points[0]->force += 5.0f*stiff*dl;
	}


	/* original ver.*/
	//for (int i = 0; i < point.size(); i++)
	//{
	//	Vector total = { 0.0f, 0.0f, 0.0f };
	//
	//	for (int j = 0; j < point[i].linked_spring.size(); j++)
	//	{
	//		float stiff = point[i].linked_spring[j]->stiffness;
	//		float len = point[i].linked_spring[j]->length;
	//		Vector L = point[i].linked_spring[j]->points[1]->position - point[i].linked_spring[j]->points[0]->position;
	//
	//		if (point[i].point_num == point[i].linked_spring[j]->points[1]->point_num)
	//			L = -1.0f*L;
	//
	//		Vector l = L;
	//		l.normalize();
	//
	//		total += -10.0f*stiff*(L - len*l);
	//	}
	//
	//	point[i].force += total;
	//}
}

void MassSpringSystem::TotalForce()
{
	ExternalForce();
	InternalForce();
}

void MassSpringSystem::Acceleration()
{
	for (int i = 0; i < point.size(); i++)
	{
		point[i].acceleration = point[i].force / point[i].mass;
	}
}

void MassSpringSystem::Velocity()
{
	for (int i = 0; i < point.size(); i++)
	{
		point[i].velocity = point[i].velocity + dt*point[i].acceleration;
	}
}

void MassSpringSystem::Position()
{
	for (int i = 0; i < point.size(); i++)
	{
		if (point[i].fixed == true) continue;

		point[i].position = point[i].position + dt*point[i].velocity;

		point[i].previous = point[i].position;
	}
}

void MassSpringSystem::SpringConstraint()
{
	for (int j = 0; j < spring.size(); j++)
	{
		Vector dir = spring[j].points[1]->position - spring[j].points[0]->position;
		dir.normalize();

		if (spring[j].length >(1.0f + toque)*spring[j].initial_length || spring[j].length < (1.0f - toque)*spring[j].initial_length)
		{
			spring[j].points[0]->force += 2.0f*dir;
			spring[j].points[1]->force -= 2.0f*dir;

			if (spring[j].points[0]->fixed == true) spring[j].points[1]->force = 2.0f*spring[j].points[1]->force;
			if (spring[j].points[1]->fixed == true) spring[j].points[0]->force = 2.0f*spring[j].points[0]->force;
		}
	}
}

void MassSpringSystem::WorkingWind(int iter_start, int iter_end)
{
	if (iter >= iter_start && iter < iter_end)
	{
		wind_velo = wind;
		windy = true;
	}
	else
	{
		wind_velo = { 0.0f, 0.0f, 0.0f };
		windy = false;
	}
}

void MassSpringSystem::FindLikedSpring()
{
	for (int j = 0; j < spring.size(); j++)
		for (int i = 0; i < point.size(); i++)
		{
			if (spring[j].points[0]->point_num == point[i].point_num || spring[j].points[1]->point_num == point[i].point_num)
				point[i].linked_spring.push_back(&spring[j]);
		}
}

void MassSpringSystem::SpringLength()
{
	for (int j = 0; j < spring.size(); j++)
	{
		Vector dp = spring[j].points[1]->position - spring[j].points[0]->position;
		float len = dp.length();

		spring[j].length = len;
	}
}

void MassSpringSystem::CloseBottom(float bottom)
{
	for (int i = 0; i < point.size(); i++)
	{
		if (point[i].position.y < bottom)
		{
			point[i].position.y = bottom;
			point[i].velocity.y *= -0.8f;
		}
	}
}