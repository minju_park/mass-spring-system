#include "Vector.h"
#include <math.h>

float Vector::length()
{
	return sqrt(x*x + y*y + z*z);
}

float Vector::norm()
{
	return sqrt(x*x + y*y + z*z);
}

void Vector::normalize()
{
	float k = norm();

	x /= k;
	y /= k;
	z /= k;
}

Vector Vector::cross(Vector v)
{
	Vector A;

	A.x = y * v.z - z * v.y;
	A.y = z * v.x - x * v.z;
	A.z = x * v.y - y * v.x;

	return A;
}

void Vector::equal(float k)
{
	x = k;
	y = k;
	z = k;
}

bool Vector::isEqual(Vector v)
{
	if (x == v.x && y == v.y && z == v.z) return true;
	else return false;
}
